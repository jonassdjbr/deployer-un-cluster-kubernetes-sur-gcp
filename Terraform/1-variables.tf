################################
## Variables du GCP  Provider##
################################

# GCP authentication variables


variable "gcp_credentials" {
    type = string
    description = "Les logins pour les services GCP"
}

variable "gcp_project_id" {
    type = string
    description = "le nom du projet gcp"
}

#############################
##      GKE - Variables    ##
#############################

variable "gcp_region" {
    type = string
    description = "La region du projet gcp"
}

variable "gke_cluster_name" {
    type = string
    description = "nom du cluster kubernetes"
}

variable "gke_zones" {
    type = list(string)
    description = "Liste des zones pour le Cluster GKE"
}

variable "gke_network" {
    type = string
    description = "Le nom du reseau VPC "
}

variable "gke_subnetwork" {
    type = string
    description = "Le nom du sous-reseau VPC "
}

variable "gke_nodepool_name" {
    type = string
    description = "Le nom du node-pool "
}

variable "gke_machine_type" {
    type = string
    description = "Le type de machine utilisé "
}

variable "gke_service_account_name" {
    type = string
    description = "Le nom du compte de service GKE"
}

variable "gke_disk_size_gb" {
    type = number
    description = "la taille du disque gke"
}