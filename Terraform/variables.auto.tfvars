####################
#Variables communes#
####################
gcp_project_id = "stoked-depth-322522"
gcp_region = "eu-central2"
gke_zones = ["europe-central2-a"]


##################
# Authentication #
##################
gcp_credentials = "stoked-depth-322522-5f8a17d17005.json"
gke_service_account_name = "terraform@stoked-depth-322522.iam.gserviceaccount.com"


################
# GKE Cluster #
################

gke_cluster_name = "clustertest"
gke_network = "default"
gke_subnetwork = "default"
gke_nodepool_name= "nodetest"
gke_machine_type = "e2-small"
gke_disk_size_gb = "100"

