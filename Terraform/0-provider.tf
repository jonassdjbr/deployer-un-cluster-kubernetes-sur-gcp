# Pour une modification ou Amélioration : https://registry.terraform.io/providers/hashicorp/google/latest/docs
provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  credentials = file(var.gcp_credentials)
}

